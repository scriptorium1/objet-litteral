let movie = {
    title: "The Dark Knight",
    releaseYear: 2008,
    director: "Christopher Nolan",
    actors: ["Christian Bale", "Heath Ledger", "Gary Oldman"],
    favouriteMovie: true,
    rating: "9,1",
    duration: 0,
    // Méthode setDuration avec les deux paramètres requis: heures et minutes
    setDuration(hours, minutes) {
        // Conversion de la durée en minutes et sauvegarde dans la propriété duration    
        this.duration = hours * 60 + minutes;
    }
}
// Appel de la méthode setDuration avec un film de 2h32 minutes
movie.setDuration(2, 32);
// Affichage de la durée du film - et de son nom, grâce à la propriété duration
console.log(`The movie ${movie.title} lasts ${movie.duration} minutes long`);